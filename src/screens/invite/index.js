import React, { Component } from 'react';
import { View, Image, TouchableOpacity, ScrollView } from 'react-native';
import {
    Container,
    Row,
    Col,
    Content,
    Text,
    Form,
    Grid,
    Thumbnail,
    Item,
    Input,
    Footer
} from 'native-base'
import styles from "./styles";
import Modal from 'react-native-modalbox'
import { contacts } from './../../consts/data/contacts'
import { responsiveWidth } from 'react-native-responsive-dimensions';

class Invite extends Component {
    constructor(props) {
        super(props);
        this.state = {
            contacts: contacts,
            is_selected:false
        };
    }

    render() {
        let invite = require('./../../assets/invite/invite.png');
        return (
            <Container style={styles.container}>
                <Row size={20} style={styles.close}>
                    {/* <Col size={20} style={styles.center}>
                        <Image
                            source={back}
                            style={styles.backImg}
                        />
                    </Col> */}
                    <Col size={80} style={styles.titleView}>
                        <Text style={styles.title}>Invite Friends</Text>
                    </Col>
                </Row>
                <Row size={50} style={styles.titleDesc}>
                    <Form>
                        <Text style={styles.desc}>Get party started by</Text>
                        <Text style={styles.desc}>inviting your closest friends to</Text>
                        <Text style={styles.desc}>your Junto.</Text>
                    </Form>
                </Row>
                <Row size={30} style={styles.center}>
                    <TouchableOpacity onPress={() => this.refs.contacts.open()}>
                        <Image
                            source={invite}
                            style={styles.inviteImg}
                        />
                    </TouchableOpacity>
                </Row>
                {this.renderModal()}
            </Container>
        );
    }

    renderModal() {
        let { is_selected } = this.state;
        let done = require('./../../assets/junto/done.png')
        return (
            <Modal
                style={[styles.modal]}
                ref={'contacts'}
                swipeToClose={true}
                position={'bottom'}
                onClosed={()=>this.setState({is_selected:false})}

            >
                {/* <Text >testing here</Text> */}
                <ScrollView
                    horizontal={true}
                    showsHorizontalScrollIndicator={false}
                >
                    {this.renderContacts()}
                </ScrollView>
                {!is_selected ?(
                    <Item rounded style={[styles.center,styles.searchBox]}>
                    <Input 
                     placeholder='Search'
                     onChangeText={(text)=>this.setState({search:text})}
                    />
                </Item>
                ):
                (
                  <Footer style={styles.doneImg}>
                  <TouchableOpacity onPress={()=>this.onDone()} >  
                
                  <Image
                    source={done}
                    style={{resizeMode:'cover',width:responsiveWidth(100)}}
                  />  
                  </TouchableOpacity>
                  </Footer>
                 )
            }
                
            </Modal>
        )
    }

    async onDone () {
        await this.refs.contacts.close();
        this.props.navigation.navigate('Group')
    }
    renderContacts() {
        let { contacts } = this.state
        return contacts.map((item, index) => {
            let source = item.is_selected?item.selected:item.image;
            return (
                <TouchableOpacity onPress={()=>this.onContactClicked(item,index)}>
                    <Form style={[styles.center, styles.contactItem]}>
                        <Thumbnail
                            source={source}
                        />
                        <Text style={styles.contactItemText}>{item.name}</Text>
                    </Form>
                </TouchableOpacity>
            )
        })
    }
    onContactClicked(item,index) {
       let { contacts } = this.state;
       
       contacts.map((data)=>{
           if(item.id!=data.id) {
               data.is_selected = false;
           }
           else{
            data.is_selected = true;
           }
       })
       this.setState({
           contacts:contacts,
           is_selected:true
       })
    }
}

export default Invite;
