import { BACKGROUND_SPLASH, ORANGE_TEXT, LIGHT_SKIP, BLACK } from './../../consts/colors'
import { StyleSheet } from 'react-native'
import {
    responsiveFontSize,
    responsiveHeight,
    responsiveWidth
} from "react-native-responsive-dimensions";

export default styles = StyleSheet.create({
    container: {
        backgroundColor: BACKGROUND_SPLASH
    },
    backImg: {
        resizeMode: 'contain',
        width: responsiveWidth(6),
        height: responsiveWidth(6)
    },
    center: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    title: {
        color: ORANGE_TEXT,
        fontFamily: 'Avenir-Heavy',
        fontSize: responsiveFontSize(3)
    },
    titleView: {
        alignItems: 'flex-start',
        justifyContent: 'center',
        paddingHorizontal:responsiveWidth(5)
    },
    titleDesc: {
        alignItems: 'flex-start',
        paddingHorizontal:responsiveWidth(5)
    },
    desc:{
        fontFamily:'Avenir-Medium',
        fontSize:responsiveFontSize(2.5)
    },
    inviteImg:{
        resizeMode:'contain'
    },
    modal:{
        width:responsiveWidth(100),
        height:responsiveHeight(40),
        borderTopLeftRadius:responsiveWidth(5),
        borderTopRightRadius:responsiveWidth(5),
        paddingTop:responsiveHeight(5),
        paddingHorizontal:responsiveWidth(5),
        backgroundColor:'#FDF4F0'
    },
    contactItem:{
        width:responsiveWidth(23)
    },
    contactItemText:{
        paddingVertical:responsiveHeight(2),
        fontFamily:'Avenir-Heavy',
        fontSize:responsiveFontSize(2)
    },
    searchBox:{
        borderColor:BLACK,
        borderWidth:responsiveWidth(1),
        paddingHorizontal:responsiveWidth(2),
        marginBottom:responsiveHeight(4)
    },
    doneImg:{
        position:'absolute',
        bottom:0
    }
})