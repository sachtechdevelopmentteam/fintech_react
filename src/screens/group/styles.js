import { BACKGROUND_SPLASH, WHITE,  } from './../../consts/colors'
import { StyleSheet } from 'react-native'
import {
    responsiveFontSize,
    responsiveHeight,
    responsiveWidth
} from "react-native-responsive-dimensions";

export default styles = StyleSheet.create({
    container: {
        backgroundColor: BACKGROUND_SPLASH
    },
    back:{
        marginVertical:responsiveWidth(10),
        marginHorizontal:responsiveWidth(10),
        backgroundColor:WHITE,
        borderRadius:responsiveWidth(5)
    },
    headerRow:{
        paddingHorizontal:responsiveWidth(3),
        paddingVertical:responsiveWidth(3),
    },
    left:{
        alignItems:'flex-end'   
    },
    center:{
        justifyContent:'center',
        alignItems:'center'
    },
    groupImg:{
        width:responsiveWidth(4),
        height:responsiveWidth(4),
        resizeMode:'cover'
    }
})