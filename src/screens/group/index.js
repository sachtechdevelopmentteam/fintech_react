import React, { Component } from 'react';
import { View, Image,TouchableOpacity } from 'react-native';
import {
    Container,
    Row,
    Col,
    Content,
    Text,
    Form,
    Grid
} from 'native-base'
import styles from "./styles";

class Group extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    let setting = require('./../../assets/group/settings.png')
    let down = require('./../../assets/group/down.png')
    let group = require('./../../assets/group/group.png')
    return (
      <Container style={styles.container} >
       <Col style={styles.back} size={100}>
         <Row size={10} >
            <Col size={10} style={styles.headerRow}>
            <TouchableOpacity onPress={()=>this.props.navigation.goBack(null)}>
              <Image
               style={styles.groupImg}
               source={down}
              />
              </TouchableOpacity>
            </Col>
            <Col size={90} style={[styles.left,styles.headerRow]}>
            <Image
               style={styles.groupImg}
               source={setting}
              />
            </Col>
         </Row>
         <Row size={10} style={styles.center}>
           <Text>Avengers</Text>
         </Row>
         <Row size={80} style={styles.center}>
         <Image
               style={{resizeMode:'contain'}}
               source={group}
              />
         </Row>
       </Col>
      </Container>
    );
  }
}

export default Group;
