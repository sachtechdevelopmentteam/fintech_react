import { responsiveFontSize,responsiveHeight,responsiveWidth } from "react-native-responsive-dimensions";
import { StyleSheet } from 'react-native'
import { BACKGROUND_SPLASH,ORANGE_TEXT,LIGHT_SKIP } from './../../consts/colors'
export default styles = StyleSheet.create({
    container:{
      backgroundColor:BACKGROUND_SPLASH
    },
    close:{
        alignItems:'flex-end',
        justifyContent:'flex-end',
        paddingHorizontal:responsiveWidth(5)
    },
    closeImg:{
        resizeMode:'cover',
        width:responsiveWidth(5),
        height:responsiveWidth(5)
    },
    circle:{
        justifyContent:'center',
        alignItems:'flex-end'
    },
    circleImg:{
        resizeMode:'cover',
        width:responsiveWidth(17),
        height:responsiveWidth(17)
    },
    center:{
        justifyContent:'center',
        alignItems:'center'
    },
    title:{
        fontFamily:'Avenir-Heavy',
        color:ORANGE_TEXT,
        fontSize:responsiveFontSize(3)
    },
    desc:{
        fontFamily:'Avenir-Medium',
        fontSize:responsiveFontSize(2.5)
    },
    descImg:{
        justifyContent:'center',
        alignItems:'flex-start',
        fontSize:responsiveFontSize(2)
    },
    skip:{
        // fontFamily:'CenturyGothic-Bold',
        fontSize:responsiveFontSize(2.5),
        color:LIGHT_SKIP
    },
    goImg:{
        resizeMode:'contain',
        
    },
})