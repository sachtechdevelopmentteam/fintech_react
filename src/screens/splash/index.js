import React, { Component } from 'react';
import { Image , TouchableOpacity } from 'react-native';
import {
  Container,
  Row,
  Col,
  Content,
  Text,
  Form,
  Grid
} from 'native-base'
import styles from './styles'

class Splash extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    let close = require('./../../assets/splash/close.png');
    let circle = require('./../../assets/splash/circle.png');
    let go = require('./../../assets/splash/gobutton.png');
    return (
      <Container style={styles.container}>
           
            {/* <Content> */}
            <Row size={10} style={styles.close}>
              <Image
                source={close}
                style={styles.closeImg}
              />
            </Row>
            <Row size={10} style={styles.center}>
              <Text style={styles.title}>Create a Junto</Text>
            </Row>
            <Row size={15} style={styles.center}>
              <Image
                source={circle}
                style={styles.circleImg}
              />
            </Row>
            <Row size={15} style={styles.center}>
              <Form style={styles.center}>
              <Text style={styles.desc}>Pool your feature earnings with</Text>
              <Text style={styles.desc}>your most trusted peers.</Text>
              </Form>
            </Row>
            <Row size={15} style={styles.circle}>
             <TouchableOpacity onPress={()=>this.gotoRoute('Invite')}> 
             <Text style={styles.skip}>SKIP</Text>
            </TouchableOpacity>
            </Row>
            <Row size={30} style={styles.center}>
              <TouchableOpacity onPress={()=>this.gotoRoute('CreateJunto')}>
              <Image
              source={go}
              style={styles.goImg}
              />
              </TouchableOpacity>
            </Row>
            
          
            
          {/* </Content> */}
            
          
      </Container>
    );
  }

  gotoRoute(route) {
   this.props.navigation.navigate(route);
  }
}

export default Splash;
