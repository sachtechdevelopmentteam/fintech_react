import React, { Component } from 'react';
import { View, Image, TouchableOpacity, TextInput } from 'react-native';

import {
    Container,
    Row,
    Col,
    Content,
    Text,
    Form,
    Grid,
    Item,
    Label,
    Footer,
    Icon,
    Input
} from 'native-base'
import styles from "./styles";
import Modal from 'react-native-modalbox'

// import { black } from 'ansi-colors';
// import { TouchableOpacity } from 'react-native-gesture-handler';

class CreateJunto extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            percentage: '',
            time: '',
            milestone: '',
            message: [],
            isFocus: false
        };
    }

    render() {
        let back = require('./../../assets/junto/back.png');
        let info = require('./../../assets/junto/info.png');
        let done = require('./../../assets/junto/done.png');
        let { name, percentage, time, milestone } = this.state;
        return (
            <Container style={styles.container}>
                <Content>
                    <Row size={20} style={styles.close}>
                        <Col size={20} style={styles.center}>
                            <TouchableOpacity onPress={() => this.props.navigation.goBack(null)}>
                                <Image
                                    source={back}
                                    style={styles.backImg}
                                />
                            </TouchableOpacity>
                        </Col>
                        <Col size={80} style={styles.titleHeader}>
                            <Text style={styles.title}>Create Your Junto</Text>
                        </Col>
                    </Row>

                    <Row size={15} style={styles.card}>

                        <Col size={90} style={styles.titleView}>
                            {/* <Text style={styles.cardText}>Junto Name</Text> */}
                            <Item floatingLabel style={{ borderBottomWidth: 0, padding: 0 }}>
                                <Label>Junto Name</Label>
                                <Input
                                    onChangeText={(text) => this.setState({ name: text })}
                                    underlineColorAndroid={'transparent'}
                                    selectionColor={"#000"}
                                    value={this.state.name}
                                />
                            </Item>
                        </Col>

                    </Row>

                    <Row size={15} style={styles.card}>

                        <Col size={90} style={styles.titleView}>
                            {/* <Text style={styles.cardText}>Percentage</Text> */}
                            <Item floatingLabel style={{ borderBottomWidth: 0 }}>
                                <Label style={{color:'#000000'}}>Percentage</Label>
                                <Input
                                    onChangeText={(text) => this.setState({ percentage: text })}
                                    underlineColorAndroid={'transparent'}
                                    selectionColor={'Black'}
                                    value={this.state.percentage}
                                />
                            </Item>
                        </Col>
                        <Col size={10} style={styles.infoView}>
                            <TouchableOpacity onPress={() => this.onModalOpen('percentage')}>
                                <Image
                                    source={info}
                                    style={styles.infoImg}
                                />
                            </TouchableOpacity>
                        </Col>
                    </Row>
                    <Row size={15} style={styles.card}>

                        <Col size={90} style={styles.titleView}>
                            {/* <Text style={styles.cardText}>Time</Text> */}
                            <Item floatingLabel style={{ borderBottomWidth: 0 }}>
                                <Label style={{color:'#000000'}}>Time</Label>
                                <Input
                                    onChangeText={(text) => this.setState({ time: text })}
                                    underlineColorAndroid={'transparent'}
                                    selectionColor={'Black'}
                                    value={this.state.time}

                                />
                            </Item>
                        </Col>
                        <Col size={10} style={styles.infoView}>
                            <TouchableOpacity onPress={() => this.onModalOpen('time')}>
                                <Image
                                    source={info}
                                    style={styles.infoImg}
                                />
                            </TouchableOpacity>
                        </Col>
                    </Row>
                    <Row size={15} style={styles.card}>

                        <Col size={90} style={styles.titleView}>
                            <Item floatingLabel style={{ borderBottomWidth: 0, flexDirection: 'row-reverse' }}>
                                <Label style={{color:'#000000'}}>Income Milestone</Label>
                                <Input
                                    leftIcon={{ type: 'font-awesome', name: 'chevron-left' }}
                                    onChangeText={(text) => this.setState({ milestone: text })}
                                    onFocus={() => this.onFocus()}
                                    onBlur={() => this.onBlur()}
                                    value={this.state.milestone}
                                    keyboardType={'number-pad'}
                                />
                                {this.state.isFocus && (
                                    <Icon
                                        name='dollar'
                                        type={'FontAwesome'}
                                        style={styles.icon}
                                    />
                                )}


                            </Item>
                        </Col>
                        <Col size={10} style={styles.infoView}>

                            <TouchableOpacity onPress={() => this.onModalOpen('milestone')}>
                                <Image
                                    source={info}
                                    style={styles.infoImg}
                                />
                            </TouchableOpacity>
                        </Col>
                    </Row>

                </Content>
                {this.renderModal()}
                <Footer style={[styles.center,styles.footer]}>
                    {name != "" && percentage != "" && time != "" && milestone != "" && (
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('Invite')}>
                            <Image
                                source={done}
                                style={styles.doneImg}
                            />
                        </TouchableOpacity>

                    )}
                </Footer>
            </Container>
        );
    }

    onFocus() {
        this.setState({
            isFocus: true
        })
    }
    onBlur() {
        this.setState({
            isFocus: false
        })
    }
    onModalOpen(type) {
        let message = []
        if (type === 'percentage') {
            message = ['What share of your future', 'earnings do you want to', 'pool with your Junto ?'];
        }
        else if (type === 'time') {
            message = ['Choose how long you want', 'your Junto agreement to', 'last for.'];
        }
        else if (type === 'milestone') {
            message = ['Choose how much each', 'member must take for', 'disbursement to begin.'];
        }
        this.setState({
            message: message
        }, () => {
            this.refs.info.open();
        })
    }

    renderModal() {
        let { message } = this.state
        return (
            <Modal
                style={[styles.modal, styles.center]}
                ref={'info'}
                swipeToClose={true}

            >
                {message.map((item) => {
                    return (
                        <Text style={styles.modalText}>{item}</Text>
                    )
                })}

            </Modal>
        )
    }
}

export default CreateJunto;
