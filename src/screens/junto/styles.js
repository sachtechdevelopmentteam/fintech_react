import {
    responsiveFontSize,
    responsiveHeight,
    responsiveWidth
} from "react-native-responsive-dimensions";
import { StyleSheet } from 'react-native'
import { BACKGROUND_SPLASH, ORANGE_TEXT, LIGHT_SKIP,GREY } from './../../consts/colors'
export default styles = StyleSheet.create({
    container: {
        backgroundColor: BACKGROUND_SPLASH
    },
    backImg: {
        resizeMode: 'contain',
        width: responsiveWidth(6),
        height: responsiveWidth(6)
    },
    center: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    titleView: {
        alignItems: 'center',
        // justifyContent: 'center',
        paddingHorizontal:responsiveWidth(2),
        paddingVertical:responsiveHeight(2.9),
        // padding:responsiveWidth(2)
    },
    titleHeader: {
        alignItems: 'flex-start',
        justifyContent: 'center',
        padding:responsiveWidth(2)
    },

    title: {
        color: ORANGE_TEXT,
        fontFamily: 'Avenir-Heavy',
        fontSize: responsiveFontSize(3)
    },
    card:{
        borderRadius:responsiveWidth(4),
        backgroundColor:'#ffffff',
        height:responsiveHeight(15),
        marginHorizontal: responsiveWidth(5),
        marginVertical: responsiveWidth(3),
        elevation:3
    },
    cardText:{
        fontSize:responsiveFontSize(2.5),
        fontFamily:'Avenir-Black'
    },
    cardImgView:{
     alignItems:'flex-end',
     justifyContent:'flex-end'
    },
    infoView:{
        alignItems:'center',
        paddingHorizontal:responsiveWidth(3),
        paddingVertical:responsiveWidth(3)
    },
    infoImg:{
        resizeMode:'cover',
        width:responsiveWidth(5),
        height:responsiveWidth(5)
    },
    doneImg:{
        resizeMode:'cover',
        width:responsiveWidth(100)
    },
    modal:{
        width:responsiveWidth(80),
        height:responsiveHeight(30),
        borderRadius:responsiveWidth(4),
        paddingHorizontal:responsiveWidth(7)
    },
    modalText:{
        color:GREY,
        fontFamily:'Avenir-Roman',
        fontSize:responsiveFontSize(2.5),
        textAlign:'left'
    },
    icon:{
        color:GREY,
        fontSize:responsiveFontSize(2.1)
    },
    footer:{
        backgroundColor:'transparent'
    } 
})