export const BACKGROUND_SPLASH = "#fdf4f0";
export const ORANGE_TEXT = "#f58a6a";
export const LIGHT_SKIP = "#e1d5c8";
export const GREY = "#b0c0d0";
export const BLACK = "#000000";
export const WHITE = "#ffffff";
