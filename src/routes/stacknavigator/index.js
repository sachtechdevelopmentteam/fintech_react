import SplashScreen from "./../../screens/splash";
import CreateJunto from "./../../screens/junto";
import Invite from "./../../screens/invite";
import Group from "./../../screens/group";
import { createAppContainer,createStackNavigator } from 'react-navigation'
const StackNavigation = createStackNavigator({
    SplashScreen:SplashScreen,
    CreateJunto:CreateJunto,
    Invite:Invite,
    Group:Group,
},{
    initialRouteName: 'SplashScreen',
    headerMode:'none'
})
export default createAppContainer(StackNavigation);