/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Fragment} from 'react';
import AppNavigator from './src/routes/stacknavigator';
const App = () => {
  return (
    <AppNavigator/>
  );
};

export default App;
